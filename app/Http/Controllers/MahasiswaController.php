<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    //CREATE
    public function save(Request $req){
        $nama = $req->nama;
        $nim = $req->nim;
        $email = $req->email;
        $no_telp = $req->no_telp;
        $prodi = $req->prodi;
        $jurusan = $req->jurusan;
        $fakultas = $req->fakultas;

        DB::table('mahasiswa')->insert(
            [
                'nama'=>$nama,
                'nim'=>$nim,
                'email'=>$email,
                'no_telp'=>$no_telp,
                'prodi'=>$prodi,
                'jurusan'=>$jurusan,
                'fakultas'=>$fakultas,
            ]
        );
        return redirect('/mahasiswa');
    }
    // READ
    public function reg_mahasiswa(){
        $hasil = DB::table('mahasiswa')->get();
        return view('/mahasiswa',['data_mhs'=>$hasil]);
    }

    //UPDATE
    public function edit($id)
    {
        $data_mhs = DB::table('mahasiswa')->where('id', $id)->get();
        return view('/edit', ['data_mhs' => $data_mhs]);
    }
   
    public function update(Request $req)
    {
        DB::table('mahasiswa')->where('id', $req->id)->update([
            'nama'=>$req->nama,
            'nim'=>$req->nim,
            'email'=>$req->email,
            'no_telp'=>$req->no_telp,
            'prodi'=>$req->prodi,
            'jurusan'=>$req->jurusan,
            'fakultas'=>$req->fakultas,
        ]);
        return redirect('/mahasiswa');
    }

    //DELETE
    public function delete($id){
        DB::table('mahasiswa')->where('id', $id)->delete();

        return redirect('/mahasiswa');
    }
 
}
